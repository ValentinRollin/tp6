package courriel;

public class Courriel {
	private String mailDest;
	private String titre;
	private String corps;
	private String cheminPJ;
	
	public Courriel(String mailDest, String titre, String corps, String cheminPJ) {
		super();
		this.mailDest = mailDest;
		this.titre = titre;
		this.corps = corps;
		this.cheminPJ = cheminPJ;
	}

	public Courriel(String mailDest, String titre, String corps) {
		super();
		this.mailDest = mailDest;
		this.titre = titre;
		this.corps = corps;
	}
	
	public Courriel(String mailDest) {
		super();
		this.mailDest = mailDest;
	}

	public String getMailDest() {
		return mailDest;
	}

	public void setMailDest(String mailDest) {
		this.mailDest = mailDest;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getCorps() {
		return corps;
	}

	public void setCorps(String corps) {
		this.corps = corps;
	}

	public String getCheminPJ() {
		return cheminPJ;
	}

	public void setCheminPJ(String cheminPJ) {
		this.cheminPJ = cheminPJ;
	}
	
	public boolean mailValide() {
		String regexMail="^[A-Za-z0-9+_.-]+@(.+)$";
		return this.getMailDest().matches(regexMail);
	}
	
	public boolean existTitre() {
		return this.getTitre()!=null;
	}
	
	public boolean existPJ() {
		String regexPJ = "(.*)+PJ+(.*)";
		String regexJoint = "(.*)+joint+(.*)";
		String regexJointe = "(.*)+jointe+(.*)";
		if (this.getCorps().matches(regexPJ) || this.getCorps().matches(regexJoint) || this.getCorps().matches(regexJointe) )
			return (this.getCheminPJ() != null);
		else
			return true;
	}
	
	public String envoyer() {
		if ((this.mailValide()) && (this.existTitre()) && (this.existPJ())) {
			return "Envoyé !";
		}
		else {
			return "Mail non valide !";
		}
	}
}
