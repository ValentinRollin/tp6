package courriel;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class TestCourriel {
	Courriel c1;
	Courriel c2;
	Courriel c3;
	
	@BeforeEach
	void setUp() throws Exception {
		c1 = new Courriel("mateo@orange.fr","titre","test mail");
		c2 = new Courriel("dan@orange.fr","mailPJ","test mail avec PJ","chemin");
		c3 = new Courriel("mateoorange.fr",null,"test mail pj sans pièce jointe");
	}

	@Test
	void testMailValide() {
		assertTrue(c1.mailValide(),"mail invalide");
		assertTrue(c2.mailValide(),"mail invalide");
		assertFalse(c3.mailValide(),"mail invalide");
	}
	
	@Test
	void testExistTitre() {
		assertTrue(c1.existTitre(),"Pas de titre");
		assertTrue(c2.existTitre(),"Pas de titre");
		assertFalse(c3.existTitre(),"Pas de titre");
		
	}
	
	@Test
	void testExistPJ() {
		assertTrue(c1.existPJ(),"pas de PJ");
		assertTrue(c2.existPJ(),"pas de PJ");
		assertFalse(c3.existPJ(),"pas de PJ");
	}
	
	@ParameterizedTest
	@ValueSource(strings = { "mat#eo@oangefr", "mateoorange.fr"})
	
	void test(String mail) {
		Courriel c = new Courriel(mail);
		assertFalse(c.mailValide());
	}


}
